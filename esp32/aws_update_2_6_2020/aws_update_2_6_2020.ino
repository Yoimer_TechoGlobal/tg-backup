#include<WiFi.h>
#include<AWS_IOT.h>
#include <OneWire.h>

//OTA web update 
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <Update.h>
#include <ArduinoJson.h>
#include <HTTPClient.h>
#include <ESP32httpUpdate.h>

//BMP Sensor
#include <Arduino.h>
#include <Wire.h>
#include <BMP180I2C.h>
#define I2C_ADDRESS 0x77
//create an BMP180 object using the I2C interface
BMP180I2C bmp180(I2C_ADDRESS);

#include <IotWebConf.h>


//SET YOUR DETAILS IN THIS SECTION
#define deviceid "TG1"   //ID for WIFI AP
#define CLIENT_ID "arn:aws:iot:us-west-2:743116883108:thing/aws_temperature"// THING UNIQUE ID
#define MQTT_TOPIC "$aws/things/aws_temperature/shadow/update" //topic for the MQTT data
#define AWS_HOST "asvflvpgadc3k-ats.iot.us-west-2.amazonaws.com" // your host for uploading data to AWS
#define AWS_S3_UPDATE "https://myotaupdates.s3-us-west-2.amazonaws.com/TG1.bin" 
//END OF DETAILS SECTION

// -- Initial name of the Thing. Used e.g. as SSID of the own Access Point.
const char thingName[] = deviceid;
const char* host = deviceid;

// -- Initial password to connect to the Thing, when it creates an own Access Point.
const char wifiInitialApPassword[] = "12345678";
int count_fails = 0;

DNSServer dnsServer;
WebServer server(80);

IotWebConf iotWebConf(thingName, &dnsServer, &server, wifiInitialApPassword);

//Dht11 Sensor
//#include <Adafruit_Sensor.h>
#include <DHT.h>
//#include <DHT_U.h>
#define DHTPIN 4    // Digital pin connected to the DHT sensor 

// Uncomment the type of sensor in use:
#define DHTTYPE    DHT11     // DHT 11
DHT dht(DHTPIN, DHTTYPE);
//uint32_t delayMS;

//MQ_2 Sensor
//Include the library
#include <MQUnifiedsensor.h>
//Definitions
#define pin 14 //Analog input 0 of your arduino
#define type 2 //MQ2
#define calibration_button 13 //Pin to calibrate your sensor
//Declare Sensor
MQUnifiedsensor MQ2(pin, type);
//Variables
float H2, LPG, CO, Alcohol, Propane, Benzene;


char buf[100];
float temp;
float humid;
float pres;

char payload[100];
char rcvdPayload[100];
bool received = false;
String my_message;
bool mqttupdate = false;
unsigned int data_interval = 1;   //Interval in minutes


//Definitions for status LEDS 
#define YELLOW 5
#define GREEN 4
bool green = false ;
bool yellow = false;
long Prev_Time = 0;
unsigned long Current_Time;

AWS_IOT aws;

const char* serverIndex = 
"<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
"<form method='POST' action='#' enctype='multipart/form-data' id='upload_form'>"
   "<input type='file' name='update'>"
        "<input type='submit' value='Update'>"
    "</form>"
 "<div id='prg'>progress: 0%</div>"
 "<script>"
  "$('form').submit(function(e){"
  "e.preventDefault();"
  "var form = $('#upload_form')[0];"
  "var data = new FormData(form);"
  " $.ajax({"
  "url: '/update',"
  "type: 'POST',"
  "data: data,"
  "contentType: false,"
  "processData:false,"
  "xhr: function() {"
  "var xhr = new window.XMLHttpRequest();"
  "xhr.upload.addEventListener('progress', function(evt) {"
  "if (evt.lengthComputable) {"
  "var per = evt.loaded / evt.total;"
  "$('#prg').html('progress: ' + Math.round(per*100) + '%');"
  "}"
  "}, false);"
  "return xhr;"
  "},"
  "success:function(d, s) {"
  "console.log('success!')" 
 "},"
 "error: function (a, b, c) {"
 "}"
 "});"
 "});"
 "</script>";

void Yellow_Status(){

  if (WiFi.status() != WL_CONNECTED)
  {
    digitalWrite(YELLOW ,LOW); 
  }
  else {
     digitalWrite(YELLOW ,HIGH); 
    }
  }

void Blink_Green(){
   digitalWrite(GREEN ,HIGH);
   delay(2000);
   digitalWrite(GREEN ,LOW);
  }

void updates(){
  mqttupdate = true; 
  t_httpUpdate_return ret = ESPhttpUpdate.update(AWS_S3_UPDATE);

        switch(ret) {
            case HTTP_UPDATE_FAILED:
                Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
                 mqttupdate = false;
                break;

            case HTTP_UPDATE_NO_UPDATES:
                Serial.println("HTTP_UPDATE_NO_UPDATES");
                 mqttupdate = false;
                break;

            case HTTP_UPDATE_OK:
                Serial.println("HTTP_UPDATE_OK");
                mqttupdate = false;
                break;
        } 
  }


void mySubCallBackHandler (char *topicName, int payloadLen, char *payLoad)
{
    strncpy(rcvdPayload,payLoad,payloadLen);
    rcvdPayload[payloadLen] = 0;
    received = true;
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(rcvdPayload);
    String msg = root["message"];
    Serial.println(msg);
    
    if (root.success()) {
      if(msg == "update"){
       Serial.println("parsing successfull");
       updates();
       return;
      }
}
}

void setup() 
{
  Serial.begin(115200);
  Serial.println();
  Serial.println("Starting up...");


pinMode(GREEN,OUTPUT);
pinMode(YELLOW,OUTPUT);

  // -- Initializing the configuration.
  iotWebConf.init();

  // -- Set up required URL handlers on the web server.
  server.on("/", handleRoot);
  server.on("/config", []{ iotWebConf.handleConfig(); });
  server.onNotFound([](){ iotWebConf.handleNotFound(); });


 if(WiFi.SSID().c_str() && WiFi.psk().c_str()){
   WiFi.begin(WiFi.SSID().c_str(),WiFi.psk().c_str());
   delay(1000);
 }

if(WiFi.status() != WL_CONNECTED)
{
 while(WiFi.status() != WL_CONNECTED){
    iotWebConf.doLoop();
    delay(1000);
    WiFi.begin(WiFi.SSID().c_str(), WiFi.psk().c_str());
    delay(1000);
  }
 
}

else{
  Serial.print("Wifi Connected");
  }

  
 /* Serial.print("\n  Initializing WIFI: Connecting to ");
  WiFi.begin(WiFi.SSID().c_str(), WiFi.psk().c_str());
  Serial.print("  ");
  while(WiFi.status() != WL_CONNECTED){
    Serial.print(".");
    delay(500);
  }*/
  
  Serial.println("\n  Connected.\n  Done");
  Serial.println("Ready.");

  Serial.println("\n  Initializing connection to AWS....");
  if(aws.connect(AWS_HOST, CLIENT_ID) == 0){ // connects to host and returns 0 upon success
    Serial.println("  Connected to AWS\n  Done.");
    
        delay(1000);
        if(0==aws.subscribe(MQTT_TOPIC,mySubCallBackHandler))
        {
            Serial.println("Subscribe Successfull");
        }
        else
        {
            Serial.println("Subscribe Failed");
            while(1);
        } 
  }
  else {
    Serial.println("  Connection failed!");
  }
  Serial.println("  Done.\n\nDone.\n");

 //BPM Initialization 
 while (!Serial);
 Wire.begin();
  //begin() initializes the interface, checks the sensor ID and reads the calibration parameters.  
  if (!bmp180.begin())
  {
    Serial.println("begin() failed. check your BMP180 Interface and I2C Address.");
    //while (1);
  }

  //reset sensor to default parameters.
  bmp180.resetToDefaults();

  //enable ultra high resolution mode for pressure measurements
  bmp180.setSamplingMode(BMP180MI::MODE_UHR);


  //Dht11 Setup
dht.begin();
  //Serial.println(F("DHTxx Unified Sensor Example"));
  // Print temperature sensor details.
  //sensor_t sensor;
  //dht.temperature().getSensor(&sensor);
  
  // Print humidity sensor details.
  // dht.humidity().getSensor(&sensor);
 
  //delayMS = sensor.min_delay / 1000;


 //MQ_2 Initialization
 //init the sensor
  MQ2.inicializar(); 
  pinMode(calibration_button, INPUT);

  /*use mdns for host name resolution*/
  if (!MDNS.begin(host)) { //http://esp32.local
    Serial.println("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }
   Serial.println("mDNS responder started");
  /*return index page which is stored in serverIndex */
  server.on("/serverIndex", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", serverIndex);
  });
  /*handling uploading firmware file */
  server.on("/update", HTTP_POST, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
    ESP.restart();
  }, []() {
    HTTPUpload& upload = server.upload();
    if (upload.status == UPLOAD_FILE_START) {
      Serial.printf("Update: %s\n", upload.filename.c_str());
      if (!Update.begin(UPDATE_SIZE_UNKNOWN)) { //start with max available size
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_WRITE) {
      /* flashing firmware to ESP*/
      if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_END) {
      if (Update.end(true)) { //true to set the size to the current progress
        Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
      } else {
        Update.printError(Serial);
      }
    }
  });
  server.begin();
     
}

void loop() 
{
  while(WiFi.status() != WL_CONNECTED){
    WiFi.begin(WiFi.SSID().c_str(), WiFi.psk().c_str());
    Yellow_Status();
    delay(500);
    aws.connect(AWS_HOST, CLIENT_ID);
    delay(500);
  }
 Yellow_Status();
 server.handleClient();
 delay(1);
  
//MQ_2 SENSOR
MQ2.update(); // Update data, the arduino will be read the voltaje in the analog pin
  
  //Rutina de calibracion - Uncomment if you need (setup too and header)
  if(calibration_button)
  {
    float R0 = MQ2.calibrate();
    MQ2.setR0(R0);
  }
  //Read the sensor and print in serial port
  //Lecture will be saved in lecture variable
  //float lecture =  MQ2.readSensor("", true); // Return LPG concentration
  // Options, uncomment where you need
  //H2 =  MQ2.readSensor("H2"); // Return H2 concentration
  LPG =  MQ2.readSensor("LPG"); // Return LPG concentration
  CO =  MQ2.readSensor("CO"); // Return CO concentration
  //Alcohol =  MQ2.readSensor("Alcohol"); // Return Alcohol concentration
  //Propane =  MQ2.readSensor("Propane"); // Return Propane concentration
  
  Serial.println("***************************");
  Serial.println("Measurements for MQ-2");
  // Serial.print("Volt: ");Serial.print(MQ2.getVoltage(false));Serial.println(" V"); 
  //Serial.print("R0: ");Serial.print(MQ2.getR0());Serial.println(" Ohm"); 
  //Serial.print("H2: ");Serial.print(H2,2);Serial.println(" ppm");
  Serial.print("LPG: ");Serial.print(LPG,2);Serial.println(" ppm");
  Serial.print("CO: ");Serial.print(CO,2);Serial.println(" ppm");
  //Serial.print("Alcohol: ");Serial.print(Alcohol,2);Serial.println(" ppm");

//dht11 code
// Delay between measurements.
  delay(20);
// Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  //float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);
  
  if (isnan(f)) {
    Serial.println(F("Error reading temperature!"));
    temp = -200;
    //return;
  }
  else {
    Serial.print(F("Temperature: "));
    Serial.print(f);
    Serial.println(F("°F"));
    temp = f;
  }
  // Get humidity event and print its value.
  if (isnan(h)){
    Serial.println(F("Error reading humidity!"));
    humid = -200;
  }
  else {
    Serial.print(F("Humidity: "));
    Serial.print(h);
    Serial.println(F("%"));
    humid = h;
  }

 //bmp code
 if (!bmp180.measurePressure())
 {
    Serial.println("could not start perssure measurement, is a measurement already running?");
    return;
  }

  //wait for the measurement to finish. proceed as soon as hasValue() returned true. 
  int retry = 0;
  do
  {
    delay(100);
    retry ++;
    if(retry >= 5){
      retry = 0;
      pres = -200;
      break;
     }
  } while (!bmp180.hasValue() and retry < 5);

  Serial.print("Pressure: "); 
  Serial.print(bmp180.getPressure());
  Serial.println(" Pa");
  
  pres = bmp180.getPressure();
  
    //create string payload for publishing
    
    String awsdata = "{\"id\":\"";
    awsdata += deviceid;
    awsdata += "\",";
    awsdata += "\"temp\":";
    awsdata += temp;
    awsdata += ",";
    awsdata += "\"humid\":";
    awsdata += humid;
    awsdata += ",";
    awsdata += "\"pres\":";
    awsdata += pres;
    awsdata += ",";
    awsdata += "\"CO\":";
    awsdata += CO;
    awsdata += ",";
    awsdata += "\"LPG\":";
    awsdata += LPG;
    awsdata += "}";

    
    char payload[100];
    Current_Time = millis();

    if(mqttupdate == false){
    
    if(Current_Time - Prev_Time > (data_interval*60*1000)){
        awsdata.toCharArray(payload, 100);
    
        Serial.println("Publishing:- ");
        Serial.println(payload);
         if(aws.publish(MQTT_TOPIC, payload) == 0){// publishes payload and returns 0 upon success
          Serial.println("Success\n");
          Blink_Green();
          Prev_Time = millis();
          count_fails = 0;
        }
        else{
          Serial.println("Failed!\n");
          count_fails ++;
          if(count_fails >= 5){
            esp_restart();
            }
      }
    }
    }  
  delay(1000);  
}

/**
 * Handle web requests to "/" path.
 */
void handleRoot()
{
  // -- Let IotWebConf test and handle captive portal requests.
  if (iotWebConf.handleCaptivePortal())
  {
    // -- Captive portal request were already served.
    return;
  }
  String s = "<!DOCTYPE html><html lang=\"en\"><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"/>";
  s += "Go to <a href='config'>configure page</a> to change WIFI settings.";
  s += "<br></br>\n";
  s += "<br></br>\n";
  s += "<title>IotWebConf 01 Minimal</title></head><body>Upload A file below to update TGboard";
  s += "<br></br>\n";
  s += serverIndex;
  s += "</body></html>\n";
  server.send(200, "text/html", s);
  
}
