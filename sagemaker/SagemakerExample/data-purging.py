#!/usr/bin/env python
# coding: utf-8

# In[1]:


import boto3
import pandas as pd
from sagemaker import get_execution_role
role = get_execution_role()
bucket='ecobpm-data-for-ml'
data_key = 'sensorsml/mldatadf.csv'
data_location = 's3://ecobpm-data-for-ml/sensorsml/mldatadf.csv'.format(bucket, data_key)
# data_key = 'original-backup/Raw_Data_-_All_Recor_1584110916002.csv'
# data_location = 's3://ecobpm-data-for-ml/original-backup/Raw_Data_-_All_Recor_1584110916002.csv'.format(bucket, data_key)
data = pd.read_csv(data_location)
pd.set_option('display.max_columns', 10)
data



# In[2]:


# gets the number of rows and colummns as a tuple
data.shape


# In[3]:


# gets the columns as a list
data.columns


# In[44]:


data.head()['63']


# In[4]:


#data.drop(data[data.TG3 == 'esp8266'].index, inplace=True)


# In[5]:


data.columns


# In[6]:


# gets first column values
# series_example = data['TG3']


# In[7]:


# let's remove esp8266, pizerohome and TGOR from TG3 column


# In[8]:


# removes esp8266 device from data
data.drop(data[data.TG3 == 'esp8266'].index, inplace=True)


# In[9]:


# removes pizerohome from data
data.drop(data[data.TG3 == 'pizerohome'].index, inplace=True)


# In[10]:


# removes TGOR from data
data.drop(data[data.TG3 == 'TGOR'].index, inplace=True)


# In[11]:


series_example = data['TG3']


# In[12]:


# prints first column values (just to confirm data is already filtered)
print(set(list(series_example)))


# In[ ]:


# creates filtered pandas Dataframe


# In[27]:


filtered = pd.read_csv('filtered_data.csv')
pd.set_option('display.max_columns', 10)
filtered


# In[ ]:


# uploads filtered file to s3 bucket


# In[28]:


bucket='ecobpm-data-for-ml' # Replace with your s3 bucket name
data_key = 'sagemaker/technoml/filtered/filtered_data.csv' # filepath in s3
boto3.Session().resource('s3').Bucket(bucket).Object(data_key).upload_file('filtered_data.csv') # file to be uploaded
print('Done uploading')


# In[14]:


# prints first column lenght
print(len(set(list(series_example))))


# In[15]:


# prints first column lenght using nunique() pandas function
print(series_example.nunique())


# In[16]:


# prints how many times a value occurs within a series (first column is this example)
series_example.value_counts()


# In[48]:


# creates a plot of this counts (first column is this example)
series_example.value_counts().plot()


# In[49]:


# creates a plot of this counts using a bar (first column is this example)
series_example.value_counts().plot(kind='bar')


# In[50]:


# creates a top-ten plot of this counts using a bar (first column is this example)
series_example.value_counts().head(10).plot(kind='bar')


# In[51]:


# generates all the numerical values, generates a count, basic statistics
data.describe()


# In[52]:


# generates all the numerical values, generates a count, basic statistics, transposing
data.describe().T

