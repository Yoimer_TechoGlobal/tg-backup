// var containerDiv = document.getElementById("analytics-data");

  embedStart=()=>{
      //wayne94david@gmail.com
      //Waynecarol94$
        let email = 'wayne94david@gmail.com'
        let password = 'Waynecarol94$'
        var awsData = {
            cognitoAuthenticatedUserName:email,
            cognitoAuthenticatedUserPassword:password,
            dashboardId: 'ca2add2c-154e-4ca0-859b-8c6c533c5d4d',
            region:'us-west-2',
            cognitoIdentityPoolId:'us-west-2:4bc95e24-fe63-4b3f-a408-166838266743',
            cognitoAuthenticatedUserPoolId:'us-west-2_9FlhoybbA',
            cognitoAuthenticatedClientId:'65uuf6bikjn2tm4lmq556opj0b',
            roleSessionName:email,
            apiGatewayUrl:'https://3uwr0p6mfl.execute-api.us-west-2.amazonaws.com/prod/getDashboardEmbedURL?',
            cognitoAuthenticatedLogins: 'cognito-idp.us-west-2.amazonaws.com/us-west-2_9FlhoybbA'
        }
        embedDashboardCognitoAuthenticated(awsData);
        // console.log(JSON.stringify(awsData))
    
    
        function onVisualLoaded() {
            var div = document.getElementById("loadedContainer");
            div.innerHTML = "Dashboard fully loaded";
        }
    
        function onError() {
            var div = document.getElementById("errorContainer");
            div.innerHTML = "your seesion has expired";
        }
    
        function embedDashboard(embedUrl) {
            var containerDiv = document.getElementById("dashboardContainer");
            var params = {
                    url: embedUrl,
                    container: containerDiv,
                    height: "1500px"
                };
                var dashboard = QuickSightEmbedding.embedDashboard(params);
                dashboard.on('error', onError);
                dashboard.on('load', onVisualLoaded);
        }
    
        function embedDashboardCognitoAuthenticated(awsData) {
            AWS.config.update({ region: awsData.region });
    
            const cognitoUser = getCognitoUser(awsData.cognitoAuthenticatedUserPoolId, awsData.cognitoAuthenticatedClientId, awsData.cognitoAuthenticatedUserName);
            const authenticationDetails = getAuthenticationDetails(awsData.cognitoAuthenticatedUserName, awsData.cognitoAuthenticatedUserPassword);
    
            cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: (result) => {
                    console.log(result);
                    const cognitoIdentity = new AWS.CognitoIdentity();
    
                    const getIdParams = {
                        IdentityPoolId: awsData.cognitoIdentityPoolId,
                        Logins: {[awsData.cognitoAuthenticatedLogins]: result.idToken.jwtToken}
                    };
    
                    cognitoIdentity.getId(getIdParams, (err, data) => {
                        if (err) {
                            console.log('Error obtaining Cognito ID.');
                        } else {
                            data.Logins = {[awsData.cognitoAuthenticatedLogins]: result.idToken.jwtToken};
    
                            cognitoIdentity.getOpenIdToken(data, (err, openIdToken) => {
                                if (err) {
                                    console.log('Error obtaining authentication token.');
                                } else {
                                    apiGatewayGetDashboardEmbedUrl(
                                        awsData.apiGatewayUrl, 
                                        awsData.dashboardId, 
                                        openIdToken.Token, 
                                        true, 
                                        awsData.roleSessionName, 
                                        false, 
                                        false
                                    );
                                }
                            });
                        }
                    });
                },
    
                onFailure: function(err) {
                    document.getElementById('loginText').innerHTML = "Check your credentials";
                    document.getElementById('loginText').style.color = 'red'
                    console.log('Error authenticating user.');
                }
            });
        }
    
        function apiGatewayGetDashboardEmbedUrl(
            apiGatewayUrl, 
            dashboardId, 
            openIdToken, 
            authenticated, 
            sessionName, 
            resetDisabled, 
            undoRedoDisabled
        ) {
            const parameters = {
                dashboardId: dashboardId,
                openIdToken: openIdToken,
                authenticated: authenticated,
                sessionName: sessionName,
                resetDisabled: resetDisabled,
                undoRedoDisabled: undoRedoDisabled
            }
    
            const myQueryString = $.param(parameters);
            apiGatewayUrl = apiGatewayUrl + myQueryString;
    
            const headers = { 'Content-Type' : 'application/json' }
    
            axios.get(apiGatewayUrl, { headers: headers})
                .then((response) => {
                    $.loading.end()
                    embedDashboard(response.data.EmbedUrl);;
                })
                .catch(function (error) {
                    console.log('Error obtaining QuickSight dashboard embed url.');
                });
        }
    
        /**
         * TODO - Move authentication functions to its own class.
         */
        function getCognitoUser(userPoolId, clientId, userName ) {
            // Step 1: Get user pool.
            const poolData = {
                UserPoolId: userPoolId,
                ClientId: clientId
            };
            const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    
            // Step 2: Get cognito user.
            const userData = {
                Username: userName,
                Pool: userPool
            };
            return new AmazonCognitoIdentity.CognitoUser(userData);
        }
    
        /**
         * TODO - Move authentication functions to its own class.
         */
        function getAuthenticationDetails(userName, userPassword) {
            const authenticationData = {
                Username: userName,
                Password: userPassword
            };
            return new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
        }  
      
}





