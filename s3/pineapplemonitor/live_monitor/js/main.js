var data_size, active_page = 1 ,counter = 0,map,infowindow,leftButton, rightButton,couroselCont,liveBtn,maximizeBtn,minimizeBtn,analyticsBtn,is_maximized = false,
    addresses_are_set = false,searchCont,historyBtn,markers,emergency_panel_shown=false,markerCluster,current_device ="",active_page = 0,geocoder,focused_device ="", 
    data = [],addresses = {},labels = [],emergency_devices=[];

    
var is_test = false;
var test_data=[
  {"id":"TG6","timestamp":1573127089364,"temperature":"75.74","humidity":"69","pressure":"95192","longitude":"-74.473358","latitude":"40.604092","LPG":"3.58","CO2":"30.34","devicestatus":{"status_device":200,"status_temperature":501,"status_humidity":200,"status_pressure":200,"status_lpg":200,"status_co2":200}},
  {"id":"TG7","timestamp":1573127089364,"temperature":"75.74","humidity":"69","pressure":"95192","longitude":"-117.439392","latitude":"33.926914","LPG":"3.58","CO2":"30.34","devicestatus":{"status_device":200,"status_temperature":200,"status_humidity":500,"status_pressure":200,"status_lpg":200,"status_co2":200}},
  {"id":"TG8","timestamp":1573127089364,"temperature":"75.74","humidity":"69","pressure":"95192","longitude":"-70.789078","latitude":"43.065041","LPG":"3.58","CO2":"30.34","devicestatus":{"status_device":200,"status_temperature":500,"status_humidity":200,"status_pressure":200,"status_lpg":200,"status_co2":200}},
  {"id":"TG5","timestamp":1573127089364,"temperature":"75.74","humidity":"69","pressure":"95192","longitude":"-81.332276","latitude":"28.78608","LPG":"3.58","CO2":"30.34","devicestatus":{"status_device":200,"status_temperature":200,"status_humidity":200,"status_pressure":200,"status_lpg":200,"status_co2":200}},
  {"id":"TG4","timestamp":1573749777927,"temperature":"77","humidity":"59","pressure":"94566","longitude":"-74.005058","latitude":"40.710255","LPG":"3.58","CO2":"30.34","devicestatus":{"status_device":200,"status_temperature":501,"status_humidity":200,"status_pressure":200,"status_lpg":200,"status_co2":200}},
  {"id":"TG2","timestamp":1573610533687,"temperature":"73.4","humidity":"61","pressure":"94871","longitude":"-76.592941","latitude":"39.296318","LPG":"3.58","CO2":"30.34","devicestatus":{"status_device":200,"status_temperature":200,"status_humidity":501,"status_pressure":200,"status_lpg":200,"status_co2":200}},
  {"id":"TG1","timestamp":1573127089364,"temperature":"75.74","humidity":"69","pressure":"95192","longitude":"-118.241600","latitude":"33.923801","LPG":"3.58","CO2":"30.34","devicestatus":{"status_device":404,"status_temperature":200,"status_humidity":200,"status_pressure":200,"status_lpg":200,"status_co2":200}}
];

/**
* Initialize Map And Auto Complete Library;
*/
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6,
    center: {lat: 35.786036, lng:-100.332118},
     })


  map1 = new google.maps.Map(document.getElementById('map1'), {
    zoom: 6,
    center: {lat: 38.9072, lng:-92.0369},
    zoomControl:false,
    disableDefaultUI: true,
  })
 
  const searchInput = document.getElementById("search-input")
  var autocomplete = new google.maps.places.Autocomplete(searchInput);
  autocomplete.bindTo('bounds', map);
  autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
  autocomplete.addListener('place_changed',function(){
    let place = autocomplete.getPlace();
    if (!place.geometry)return;
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(16); 
    }
  });
  // initialize geocorder
  geocoder = new google.maps.Geocoder();
  mapInitialized = true; 
  fetchlocations()
  setInterval(()=>{
    //uncomment in production
    fetchlocations()
    },3000)
}
/**
 * Fetch Locations
 */
fetchlocations =()=>{
  fetch("https://fs24s0yqza.execute-api.us-west-2.amazonaws.com/sensors/sensors").
    then((response)=>{
      return response.json();
    })
    .then((response)=>{
      if(markers)
          markerCluster.clearMarkers();
        return response;
    })
    .then(response=>{
      if (active_page == 0) {
        // console.log('Setting Location' )

        //uncomment in production
        setLocations(response.body);
        showEmergency(response.body); 
      }
    })
}
/**
* Start Live Update; 
*/
startLiveUpdate=()=>{
   $(".regular").slick({
      dots: false,
      // infinite: true,
      slidesToShow: 4,
      slidesToScroll: 4,
      // autoplay: true,
      // autoplaySpeed:2000,
      // speed:200,
  });
  setInterval(()=>{
    fetch("https://fs24s0yqza.execute-api.us-west-2.amazonaws.com/sensors/sensors").
    then((response)=>{
      return response.json();
    }).then((response)=>{
      if(is_test == true){
        data = test_data;
        console.log('Using TEST DATA')
      }else{
        data = response.body;
        data_size = data.length;
        console.log('Using USING API DATA')
      }
      if((counter>=0 && counter<=data_size-1) && active_page ==0){
        let lpg=data[counter]["LPG"];
        let co2=data[counter]["CO2"];
        let humidity=data[counter]["humidity"];
        let temperature= data[counter]["temperature"]
        let presuret =data[counter]["pressure"];
        let date = new Date((data[counter]["timestamp"]));
        let ftime = format_time(date)
        let fdate = format_date(date)

        if(focused_device === ""){
            if(fdate) {
              document.getElementById("date").innerText=fdate;
            }
            if(ftime) {
              document.getElementById("time").innerText=ftime;
            }
            if(lpg) {
              document.getElementById("lpg").innerText=lpg;
            }
            if(co2) {
              document.getElementById("co2").innerText=co2;
            }
            if(humidity) {
              document.getElementById("humidity").innerText=humidity;
            }
            if(temperature) {
              document.getElementById("temperature").innerText=temperature;
            }
            if(presuret) {
              document.getElementById("presurret").innerText=presuret;
            }
        }else{
          data.forEach((device,index)=>{
            if ( device['id']=== focused_device){
                  document.getElementById("date").innerText=format_date(new Date(device['timestamp']));;
                  document.getElementById("time").innerText=format_time(new Date(device['timestamp']));
                  document.getElementById("lpg").innerText=device['LPG'];
                  document.getElementById("co2").innerText=device['CO2'];
                  document.getElementById("humidity").innerText=device['humidity'];
                  document.getElementById("temperature").innerText=device['temperature'];
                  document.getElementById("presurret").innerText=device['pressure'];
                  document.getElementById('address_').innerText = addresses[device['id']]? addresses[device['id']]:'Searching...';
            }
          });
        }
      }
    }).catch(function(error){
        console.log(error)
    })
  },1000);  
  
  leftButton = document.getElementById("left-btn");
  rightButton = document.getElementById("right-btn");
  couroselCont =document.getElementById("couresel-cont");
  const searchButton = document.getElementById("search");
  const closeBtn = document.getElementById("close-btn");
  const closeBtnC = document.getElementById("close-btn-container");
  liveBtn = document.getElementById('live-monitoring-btn');
  historyBtn = document.getElementById('historical-btn');
  maximizeBtn = document.getElementById('maximize-btn-container');
  minimizeBtn = document.getElementById('minimize-btn-container');
  minimizeBtn = document.getElementById('minimize-btn-container');
  analyticsBtn = document.getElementById('analytics-btn');
  const testButton = document.getElementById('emergency-test-btn')


  leftButton.addEventListener("click",()=>clickHandler("left"));
  rightButton.addEventListener("click",()=>clickHandler("right"));
  searchButton.addEventListener("click",()=>clickHandler("search"));
  closeBtnC.addEventListener("click",()=>{ document.getElementById("emergency-container").style.display = "none"; emergency_panel_shown = false});
  liveBtn.addEventListener('click',()=>clickHandler('setlive'));
  historyBtn.addEventListener('click',()=>clickHandler('sethistory'));
  maximizeBtn.addEventListener('click',()=>clickHandler('maximize'));
  minimizeBtn.addEventListener('click',()=>clickHandler('minimize'));
  analyticsBtn.addEventListener('click',()=>clickHandler('analytics'));
  testButton.addEventListener('click',()=>clickHandler('emergency-test'));
}
/**
* Click Handler
* @params option
*/
clickHandler=(option)=>{
  switch (option) {
    case "left":
       if(counter>0){
        $('.card-button').toggleClass('flipped-up');
       };
      counter == 1? leftButton.innerHTML =  '<img src="icons/last_left_page.png" class="page_btn" alt="R"></img>':null;
      rightButton.innerHTML = '<img src="icons/right_page.png" class="page_btn" alt="R"></img>';
      counter>0? counter-- : counter=0;
      break;
    case "right":
        if(counter<data_size-1){
          $('.card-button').toggleClass('flipped-up');
        }
        counter == data_size-2? rightButton.innerHTML =  '<img src="icons/last_right_page.png" class="page_btn" alt="R"></img>':null;
        (counter<data_size-1)? ++counter: counter=data_size-1;
        leftButton.innerHTML = '<img src="icons/left_page.png" class="page_btn" alt="R"></img>';
    break;
    case "search":
      searchDevice();
    break;
    case 'setlive':
       active_page = 0;
       document.getElementById("couresel-cont").style.display = "block";
       document.getElementById("history-text").style.color = "#ffffff";
       document.getElementById("live-text").style.color = "red";
       document.getElementById("history-content").style.display = "none";
       document.getElementById("livedata-content").style.display = "";
       document.getElementById("analytics-content").style.display = "none";
       document.getElementById("analytics-text").style.color = "#ffffff";
    break;
    case 'sethistory':
       active_page = 1;
       document.getElementById("couresel-cont").style.display = "none";
       document.getElementById("live-text").style.color = "#ffffff";
       document.getElementById("history-text").style.color = "red";
       document.getElementById("history-content").style.display = "";
       document.getElementById("livedata-content").style.display = "none";
       document.getElementById("analytics-content").style.display = "none";
       document.getElementById("analytics-text").style.color = "#ffffff";
    break;
    case 'maximize':
        if(is_maximized === true){
          document.getElementById("emergency-container").style.cssText = "height:auto";
          document.getElementById("emergency-highlight").style.display = '';
          document.getElementById("emergency-details").style.display = 'none'
          is_maximized = false;
        }else if(is_maximized === false){
          document.getElementById("emergency-container").style.cssText = "height:100%;top:0";
          document.getElementById("emergency-highlight").style.display = 'none';
          document.getElementById("emergency-details").style.display = ''
          is_maximized = true;
        }
      
    break;
    case 'minimize':
          document.getElementById("emergency-container").style.cssText = "height:auto";
          document.getElementById("emergency-highlight").style.display = '';
          document.getElementById("emergency-details").style.display = 'none'
          document.getElementById("analytics-content").style.display = "none";
          document.getElementById("analytics-text").style.color = "#ffffff";
          is_maximized = false;
    break;
    case 'analytics':
        active_page = 1;
        document.getElementById("couresel-cont").style.display = "none";
        document.getElementById("live-text").style.color = "#ffffff";
        document.getElementById("history-text").style.color = "#ffffff";
        document.getElementById("analytics-text").style.color = "red";
        document.getElementById("history-content").style.display = "none";
        document.getElementById("livedata-content").style.display = "none";
        document.getElementById("analytics-content").style.display = "";
        // embedStart();
    break;
    case 'emergency-test':
     
      emergency_panel_shown = true;
      document.getElementById('emergency-container').style.display = '';
      showEmergency(test_data);
      setInterval(()=>{
        if (is_test == false ) {
            setLocations(test_data);
            is_test = true;
        }else{
          markerCluster.clearMarkers();
          setLocations(test_data);
        }
      },1000);

      setInterval(()=>{
        if (is_test == false ) {
            setLocations(test_data);
            is_test = true;
        }else{
          markerCluster.clearMarkers();
          setLocations(test_data);
        }
        showEmergency(test_data);
        // markerCluster.clearMarkers();
      },10000)
    break;
  }
}

/**
* Set Locations
* @param data
*/
setLocations=(data)=>{
  console.log('Setting Location');
  infoWindow = new google.maps.InfoWindow({
    content: document.getElementById('infowindow-content')
  });
  var icons = {
      icon1:'./icons/200.png',
      icon2:'./icons/500.png',
      icon3:'./icons/404.png',
      };
  markers = data.map(function(device, i) {
    let lat = parseFloat(device["latitude"]); 
    let lng = parseFloat(device["longitude"]); 
    return new google.maps.Marker({
      position: {lat, lng},
      title: i.toString(),
      icon: (device['devicestatus']['status_device'] === 200)?
      (
        device['devicestatus']['status_temperature']== 500
        || device['devicestatus']['status_temperature']== 501
        || device['devicestatus']['status_humidity']== 500
        || device['devicestatus']['status_humidity']== 501
        || device['devicestatus']['status_pressure']== 500
        || device['devicestatus']['status_pressure']== 501
        || device['devicestatus']['status_lpg']== 501
        || device['devicestatus']['status_lpg']== 500
        || device['devicestatus']['status_co2']== 501
        || device['devicestatus']['status_co2']== 500
      )?icons.icon2:icons.icon1
      : 
      icons.icon3
    });
  });
  markerCluster = new MarkerClusterer(map, markers,{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    markers.forEach((marker,index) => {
      var contentString = String('<div id="infowindow-content">'
                            +'<table>'
                                +'<tr id="iw-title-row" class="iw_table_row">'
                                +'<td class="iw_attribute_name">Device ID:</td>'
                                        +'<td id="iw-device-id">'+data[marker.getTitle()]['id']
                                        +'</td>'
                                        +'</tr>'
                                        +'<tr id="iw-address-row" class="iw_table_row">'
                                +'<td class="iw_attribute_name">Date:</td>'
                            +'<td id="iw-date">'+format_date(new Date(data[marker.getTitle()]['timestamp']))+'</td>'
                            +'<td class="iw_attribute_name">Time:</td>'
                            +'<td id="iw-time">'+format_time(new Date(data[marker.getTitle()]['timestamp']))+'</td>'
                            +'</tr>'
                            +'<tr id="iw-phone-row" class="iw_table_row">'
                            +'<td class="iw_attribute_name">LPG:</td>'
                            +'<td id="iw-lpg">'+data[marker.getTitle()]['LPG']+'</td>'
                            +'<td class="iw_attribute_name">Pressure:</td>'
                            +'<td id="iw-pressure">'+data[marker.getTitle()]['pressure']+'</td>'
                            +'</tr>'
                            +'<tr id="iw-rating-row" class="iw_table_row">'
                            +'<td class="iw_attribute_name">C02:</td>'
                            +'<td id="iw-c02">'+data[marker.getTitle()]['CO2']+'</td>'
                            +'<td class="iw_attribute_name">Temperature:</td>'
                            +'<td id="iw-temp">'+data[marker.getTitle()]['temperature']+' &#8457'+'</td>'
                            +'</tr>'
                            +'<tr id="iw-website-row" class="iw_table_row">'
                            +'<td class="iw_attribute_name">Humidity:</td>'
                            +'<td id="iw-humidity">'+data[marker.getTitle()]['humidity']+'</td>'
                            +'</tr>'
                            +'</table>'
                            +'</div>')
      var infowindow = new google.maps.InfoWindow({content:contentString});
      google.maps.event.addListener(marker,'mouseover', function() { infowindow.open(map, marker);});
  });
}


/** 
 * Update Courosel On Focus
*/
updateCourosel=(device)=>{
  $('.card-button').toggleClass('flipped-up');
}
/**
 * Emergency Alert
 */
function showEmergency(data){
  // check if there is any emergency in all devices then show emergency panel once
  console.log('Showing Emergency')
  data.forEach((device,index)=>{
    if ( device['devicestatus']['status_device']===404 && emergency_panel_shown == false ){
            document.getElementById('emergency-container').style.display = '';
            emergency_panel_shown = true;
           return;
    }else if( ((device['devicestatus']['status_device']===200) && emergency_panel_shown === true)){
             document.getElementById('emergency-container').style.display = 'none';
             emergency_panel_shown = false;
             console.log('Emergency Hidding');
             return;
    }
  });
  // if emergency panel is showing populate the emergency panel will all the emergencies
  if(emergency_panel_shown){
    // clean emergencies and emergency div
    document.getElementById("emergency_details_holder").innerHTML = ''
    // document.getElementById("tiles_container").innerHTML = ''
    emergency_devices = []
    $('.slider').slick('slickRemove');
    data.forEach((device,index)=>{
          displayLocation(device['id'],device['latitude'],device['longitude'])
           if( device['devicestatus']['status_device'] === 404 ){
                  //push the devices to the emergency details container
                  var node = document.createElement("div")
                  node.innerHTML = '<div id="'+'details_'+device['id']+'" style="cursor:pointer"><i class="material-icons" >warning</i><br>'+'<div>'+'ID:'+device['id']+'</div>'+'<b>Place</b>:'+((addresses[device['id']])?(addresses[device['id']]):"Searching").replace(new RegExp('"', 'g'), '')+'</div>'+'<br><b>Incident: </b>Device Offline</div>';                    
                  document.getElementById("emergency_details_holder").appendChild(node);
                  tileClickLister(device['id'],document.getElementById( 'details_'+device['id']),device['latitude'],device['longitude'],'details');
                  //push the devices to the emergency highlight container
                  let html =  '<div id="'+device['id']+'" style="cursor: pointer;position:relative;z-index:99;">'
                  +      '<img class="animated infinite swing" style="heigh:100px;position:relative;z-index:99" src="icons/clock.png" >' 
                  +  '<div style="text-align: center;">'
                  +      '<span class="animated infinite flash emergency-text">EMERGENCY!</span>'
                  +      '<br>'
                  +      '<span class="emergency-text" style="font-size:12px">'+'OFFLINE'+'</span><span class="emergency-text"></span>'
                  +  '</div>'
                  +'</div>'
                  $('.slider').slick('slickAdd',html);
                  tileClickLister(device['id'],document.getElementById(device['id']),device['latitude'],device['longitude'],'');
                  emergency_devices.push(device['id'])
                  console.log('Pushing device '+device['id']+' REASON::OFFLINE');

                  

      }else if(device['devicestatus']['status_device'] === 200 ){
           console.log('Checking instances')
           let incidences = 
             ((device['devicestatus']['status_temperature'] === 500)?" Low Temperature,":(device['devicestatus']['status_temperature'] === 501)?" High Temperature,":"") 
           +((device['devicestatus']['status_humidity'] === 500)?" Low Humidity,":(device['devicestatus']['status_humidity'] === 501)?" High Humidity,":"")
           +((device['devicestatus']['status_lpg'] === 500)?" Low LPG,":(device['devicestatus']['status_lpg'] === 501)?" High LPG,":"")
           +((device['devicestatus']['status_co2'] === 500)?" Low CO2,":(device['devicestatus']['status_co2'] === 501)?" High CO2,":"")
           +((device['devicestatus']['status_pressure'] === 500)?" Low Pressure,":(device['devicestatus']['status_pressure'] === 501)?" High Pressure,":"")
            emergency_devices.push( device['id'] )

          if (incidences.length !==0) {
            //push the devices to the emergency details container
            let node = document.createElement("div");
            node.innerHTML = '<div id="'+'details_'+device['id']+'" style="cursor:pointer; "><i class="material-icons" >warning</i><br>'+'<div>'+'ID:'+device['id']+'</div>'+'<b>Place</b>:'+((addresses[device['id']])?(addresses[device['id']]):"Searching").replace(new RegExp('"', 'g'), '')+'<br> <b>Incident: </b>'+incidences+'';                    
            document.getElementById("emergency_details_holder").appendChild(node); 
            tileClickLister(device['id'],document.getElementById( 'details_'+device['id']),device['latitude'],device['longitude'],'details');
            //push the devices to the emergency highlight container
            let html =  '<div id="'+device['id']+'" style="cursor: pointer;">'
            +      '<img class="animated infinite swing" style="heigh:100px;position:relative;z-index:99" src="icons/clock.png" >' 
            +  '<div style="text-align: center;">'
            +      '<span class="animated infinite flash emergency-text">EMERGENCY!</span>'
            +      '<br>'
            +      '<span class="emergency-text" style="font-size:12px">'+incidences+'</span><span class="emergency-text"></span>'
            +  '</div>'
            +'</div>'
            $('.slider').slick('slickAdd',html);     
            emergency_devices.push(device['id']);
            tileClickLister(device['id'],document.getElementById(device['id']),device['latitude'],device['longitude'],'');
            console.log("Pushing "+device['id'] +" REASON::"+incidences);
          } else if (incidences.length === 0) {
            console.log('No incident')
            // document.getElementById('emergency-container').style.display = 'none';
          } 
        }
    })
    console.log(addresses)
    addresses_are_set = true;
    console.log('Cycle Complete With '+ emergency_devices + ' as emergencies')
  }
}

/**
 * 
 * Tile Click Listener
 */
 tileClickLister=(device_id,tileContainer,lat,lng,origin)=>{
    tileContainer.addEventListener('click',()=>{
    focused_device = device_id;
    map.setZoom(16);
    map.setCenter(new google.maps.LatLng(lat,lng));
    updateCourosel();
    document.getElementById('address_display').style.display = "";
    if(origin ==="details"){
          document.getElementById("emergency-container").style.cssText = "height:auto";
          document.getElementById("emergency-highlight").style.display = '';
          document.getElementById("emergency-details").style.display = 'none'
          document.getElementById("analytics-content").style.display = "none";
          document.getElementById("analytics-text").style.color = "#ffffff";
          is_maximized = false;
    }
});
}

/**
 * Search Device
 */ 
searchDevice=()=>{
  let value = document.getElementById("search-val").value
  if (value.trim().length !==0 ) {
  }else{
    $('.search-input').toggleClass('null-input');
  }
}

/**
 * Format_Date
 */
 format_date=(date)=>{
   return ("0"+date.getDate()).substr(-2) + '-' + ("0" + (date.getMonth()+1)).substr(-2) + '-' +date.getFullYear();
 }
 format_time=(date)=>{
   return ("0"+date.getHours()).substr(-2) + ':' + ("0" + date.getMinutes()).substr(-2) + ':' + ("0"+ date.getSeconds()).substr(-2);
 }

/**
 * Display Location
 */ 
displayLocation=(device_id,latitude,longitude)=>{
  if (addresses_are_set === false){
    let address;
    geocoder.geocode(
        {'latLng': new google.maps.LatLng(latitude, longitude)},(results, status)=> {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    address = JSON.stringify(results[0].formatted_address);
                    console.log(address);
                   if(address){
                      addresses[device_id] = address;
                   }else{
                      address[device_id] = "Searching..."
                   }
                  }
                else  {
                    address = 'Location Not Found';
                }
            }
            else {
                console.log("Geocoder failed due to: " + status);
                address =  'Searching';
            }
        }
    );
  }
}

/**
 * 
 * Emergency Tiles
*/

/**
 * Start Live Update
 */
document.addEventListener("DOMContentLoaded",()=>startLiveUpdate());



// - blinking, from left to right
// - more test data
// - 
