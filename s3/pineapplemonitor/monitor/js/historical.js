var _startDate, _endDate, sensorKey, siteKey,searchKey;
var sites = {1:'Lake Mary'};
var sensors = {};
var tables;

// JQUERY DATE PICKER
$(function() {
     start = moment().subtract(1, 'days');
     end = moment();
    function filter(start, end) {
        $('#datepicker input').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
        _startDate = start;
        _endDate = end;
    }
    $('#datepicker').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'This Year': [moment().startOf('year'), moment().endOf('year')],
        }
    }, filter);
    filter(start, end);
});


//fetch data on load  
document.addEventListener("DOMContentLoaded",()=>{
    fetch("https://fs24s0yqza.execute-api.us-west-2.amazonaws.com/sensors/sensors").
    then((response)=>{
      return response.json();
    }).then((response)=>{
           response.body.forEach((device, i) => {
               sensors[i] = device['id'];
            });
            // console.log(JSON.stringify(sensors))
            $('#custom-select-search-1').bselect({
                data : sensors,
                search : true, 
                width : '150px',
                defaultText : "Sensor",
                closeOnSelect :true,
                elipsis :false,
            });
            //TODO::Set Sites
            $('#custom-select-search').bselect({
                data : sites,
                search : true, 
                width : '150px',
                defaultText : "Site",
                closeOnSelect :true,
                elipsis :false,
                className :"",
            });
    }).catch(err=>console.log(err))
    apiData({case:'general'},"default")
});


function apiData(params) {
    // console.log(JSON.stringify("In API Request::::::::"+ JSON.stringify(params)))
    var xhr = $.ajax({
        url: 'https://rjqozp7g80.execute-api.us-west-2.amazonaws.com/prod/gethistorical',
        type: 'GET',
        data: jQuery.param(params) ,
        success: function (response) {
            // alert(JSON.stringify(response))
            setTable(response.body)
            $('table').removeClass('loading'); 
        },
        beforeSend: function(jqXHR, settings) {
            // console.log(settings.url);
        },
        error: function () {
            // console.log("error loadng data");
        }
    });
}


function setTable(data) {
    // console.log('DATA:::::'+JSON.stringify(data));
    mydata = []
    data.forEach(device=>{
        mydata.push({date:getFormatedDate(new Date(device['timestamp'])),siteid:device['siteid'],id:device['id'],location:device['location'],
                lpg:device['lpg'],temperature:device['temperature'],humidity:device['humidity'],
                co2:device['co2'],pressure:device['pressure']
        });
    })
        tables = $('#history-table').DataTable({
        data:mydata,
        columns: [
            { data: 'date' },
            { data: 'siteid' },
            { data: 'id' },
            { data: 'location'},
            { data: 'lpg'},
            { data: 'temperature' },
            { data: 'humidity' },
            { data: 'co2' },
            { data: 'pressure' }
        ],
        "pageLength": 3,
        destroy: true,
        infor:false,
        searching: false,
        ordering:  false,
        "info": false,
        "paging": true,
        "lengthMenu": [3, 10, 25, 50, 75, 100 ],
        "pagingType": "full",
        'processing': true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': 'Loading...'
        }   
    });
    $( tables.table().body()).addClass('mdc-data-table__content');
}

// case=date&startdate=1574796207039&enddate=1575037498700
// Component Initialization
$('#filter-btn').on('click',function(){
          sensorKey = $( "#custom-select-search-1" ).find( ".bselect-default-text").text();
          siteKey = $( "#custom-select-search" ).find( ".bselect-default-text").text()

          if (sensorKey !== 'Sensor' && siteKey !== "Site") {
            //   date site sensor
            $('table').addClass('loading'); 
            apiData({case:'datedevsite',startdate:toTimestamp(_startDate),enddate:toTimestamp(_endDate), deviceid:sensorKey,site:siteKey});
          }

          if (sensorKey === 'Sensor' && siteKey === "Site") {
            $('table').addClass('loading'); 
            //   date  
            apiData({case:'date',startdate:toTimestamp(_startDate),enddate:toTimestamp(_endDate)});
          }

          if (sensorKey === 'Sensor' && siteKey !== "Site") {
            $('table').addClass('loading');
            //   date site
            apiData({case:'datesite',startdate:toTimestamp(_startDate),enddate:toTimestamp(_endDate),site:siteKey});
          }

          if (sensorKey !== 'Sensor' && siteKey === "Site") {
            $('table').addClass('loading'); 
            //   date sensor
            apiData({case:'datedevice',startdate:toTimestamp(_startDate),enddate:toTimestamp(_endDate),deviceid:sensorKey});
          }
    }
)

//overall search
$("#search-text").keyup(function(){
    _this = this;
    $.each($(".js-table tbody tr"), function() {
        if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
            $(this).hide();
        else
            $(this).show();
    });
});

//sensor search
$("#custom-select-search").keyup(function(){
    _this = this;
    $.each($(".js-table tbody tr"), function() {
        if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
            $(this).hide();
        else
            $(this).show();
    });
});

$('#refreshBtn').on('click',function() {
    $("#refreshIcon").addClass("animated infinite rotateIn");
    setTimeout(()=>{
        $( "#custom-select-search" ).find( ".bselect-default-text").text('Site');
        $( "#custom-select-search-1" ).find( ".bselect-default-text").text('Sensor');
        $("#refreshIcon").removeClass("animated infinite rotateIn");
    },1000)
});

//site search
$('#custom-select-search').on('selected.bselect',function(e,params){
    var sKey = $( "#custom-select-search" ).find( ".bselect-default-text").text();
    $("#refreshBtn").css("display", "");
})

$('#custom-select-search-1').on('selected.bselect',function(e,params){
    var senKey = $( "#custom-select-search-1" ).find( ".bselect-default-text").text()
    $("#refreshBtn").css("display", "");
});
 
function toTimestamp(strDate){
    var datum = Date.parse(strDate);
    return datum;
}

getFormatedDate=(date)=>{
        return ( 
            '<span>' + ("0" + (date.getMonth()+1)).substr(-2)  + '-' + ( "0"+date.getDate()).substr(-2) + '-' +date.getFullYear()+'</span>'+'</br>'
           +'<span>' +("0"+date.getHours()).substr(-2) + ':' + ("0" + date.getMinutes()).substr(-2) + ':' + ("0"+ date.getSeconds()).substr(-2)+'</span>'+'</br>'
        );
}